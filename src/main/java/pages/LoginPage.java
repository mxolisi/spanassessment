/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author Veco Mxolisi
 */
public class LoginPage extends BasePage {

    private final By usernameTxt = By.name("username");
    private final By passwordTxt = By.name("password");
    private final By signInBtn = By.xpath("//input[@value='sign in']");

    public LoginPage(WebDriver driver, ExtentTest extentTest) {
        super(driver, extentTest);
    }

    @Override
    public void waitForPageToLoad(int milliSeconds) {
        extentTest.pass("Waiting for the login page to load");
        waitForElementToBeClickable(usernameTxt, milliSeconds);
    }

    public void login(String username, String password) {
        extentTest.pass("Setting username to " + username);
        sendKeys(usernameTxt, username);
        extentTest.pass("Setting password to " + password);
        sendKeys(passwordTxt, password);
        extentTest.pass("Clicking SIGN IN");
        click(signInBtn);
    }
}
