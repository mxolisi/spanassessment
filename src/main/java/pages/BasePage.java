/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import com.aventstack.extentreports.ExtentTest;
import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author Veco Mxolisi
 */
public abstract class BasePage {

    protected WebDriver driver;
    protected ExtentTest extentTest;

    public BasePage(WebDriver driver, ExtentTest extentTest) {
        this.driver = driver;
        this.extentTest = extentTest;
    }

    public WebElement waitForElementToBeClickable(By lookUpForElementToWaitFor, int secondsToWait) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(secondsToWait));
        return wait.until(ExpectedConditions.elementToBeClickable(lookUpForElementToWaitFor));
    }

    public void sendKeys(By by, String keysToSend) {
        driver.findElement(by).sendKeys(keysToSend);
    }

    public void click(By by) {
        driver.findElement(by).click();
    }

    public abstract void waitForPageToLoad(int milliSeconds);
}
