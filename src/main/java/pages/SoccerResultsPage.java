/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import com.aventstack.extentreports.ExtentTest;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Veco Mxolisi
 */
public class SoccerResultsPage extends BasePage {

    private final By resultsTable = By.xpath("//table[@class='table table-striped table-condensed']");

    public SoccerResultsPage(WebDriver driver, ExtentTest extentTest) {
        super(driver, extentTest);
    }

    @Override
    public void waitForPageToLoad(int milliSeconds) {
        extentTest.pass("Waiting for the soccer results page to load");
        waitForElementToBeClickable(resultsTable, milliSeconds);
    }

    public List<Integer> getListOfTeamPoints() {
        WebElement resultsTableElement = driver.findElement(resultsTable);
        List<WebElement> dataRows = resultsTableElement.findElements(By.xpath("tbody/tr"));

        List<Integer> scores = new ArrayList<>();
        String scoreTD;

        for (int i = 0; i < dataRows.size(); i++) {
            scoreTD = dataRows.get(i).findElement(By.xpath("td[3]")).getText();
            scores.add(Integer.valueOf(scoreTD));
        }

        return scores;
    }

}
