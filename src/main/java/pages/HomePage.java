/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pages;

import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author Veco Mxolisi
 */
public class HomePage extends BasePage {

    private final By ourServicesHeader = By.xpath("//h3[text()='Our Services']");
    private final By strategyAndInnovationServiceHeader = By.xpath("//h6[text()='STRATEGY & INNOVATION']");
    private final By designAndDevelopmentServiceHeader = By.xpath("//h6[text()='DESIGN & DEVELOPMENT']");
    private final By evolutionAndAdoptionServiceHeader = By.xpath("//h6[text()='EVOLUTION & ADOPTION']");

    public HomePage(WebDriver driver, ExtentTest extentTest) {
        super(driver, extentTest);
    }

    @Override
    public void waitForPageToLoad(int milliSeconds) {
        extentTest.pass("Waiting for the home page to load");
        waitForElementToBeClickable(ourServicesHeader, milliSeconds);
    }

    public String getStrategyAndInnovationServiceHeader() {
        return driver.findElement(strategyAndInnovationServiceHeader).getText();
    }

    public String getDesignAndDevelopmentServiceHeader() {
        return driver.findElement(designAndDevelopmentServiceHeader).getText();
    }

    public String getEvolutionAndAdoptionServiceHeader() {
        return driver.findElement(evolutionAndAdoptionServiceHeader).getText();
    }

}
