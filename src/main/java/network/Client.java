/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 *
 * @author Veco Mxolisi
 */
public class Client {

    public static Response getResource(String url) {
        RestAssured.useRelaxedHTTPSValidation();
        RestAssured.baseURI = url;
        RequestSpecification request = RestAssured.given();
        return request.get();
    }

    public static Response getResource(String url, String username, String password, String authorizationHeader) {
        RestAssured.useRelaxedHTTPSValidation();
        RestAssured.baseURI = url;
        RequestSpecification request = RestAssured.given();
        request.header("Authorization", authorizationHeader);
        request.auth().basic(username, password);
        return request.get();
    }
}
