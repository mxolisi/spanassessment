/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import base.BaseOfTests;
import com.aventstack.extentreports.ExtentTest;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import lombok.Cleanup;
import static network.Client.getResource;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.v96.network.Network;
import org.openqa.selenium.devtools.v96.network.model.Headers;
import org.testng.annotations.Test;

/**
 *
 * @author Veco Mxolisi
 */
public class APITest extends BaseOfTests {

    @Test()
    public void availableEndpointsTest() {
        ExtentTest extentTest = extent.createTest("5. Available endpoints test");

        String url = BASE_URL + "/api/";

        extentTest.pass("Doing GET : " + url);
        Response response = getResource(url);

        int expectedStatusCode = 200;
        extentTest.pass("Response recieved :<p><pre>" + response.getBody().asPrettyString() + "</pre></p>");
        assertEquals(response.getStatusCode(), expectedStatusCode, "Verify status code is " + expectedStatusCode, extentTest);

        JSONObject jsonObject = new JSONObject(response.getBody().asString().trim());
        Iterator<String> keys = jsonObject.keys();
        String key;

        @Cleanup("quit")
        WebDriver driver = new ChromeDriver();

        DevTools devTools = ((ChromeDriver) driver).getDevTools();
        devTools.createSession();
        devTools.send(Network.enable(Optional.of(100000), Optional.of(100000), Optional.of(100000)));

        Map<String, Object> headers = new HashMap<>();
        headers.put("Authorization", AUTH_HEADER);
        devTools.send(Network.setExtraHTTPHeaders(new Headers(headers)));

        String tempUrl;
        boolean isServerError;
        while (keys.hasNext()) {
            key = keys.next();
            extentTest.pass("Testing resource \"" + key + "\" using GET : " + jsonObject.get(key));

            tempUrl = jsonObject.get(key).toString();
            response = getResource(tempUrl, USERNAME, PASSWORD, AUTH_HEADER);
            extentTest.pass("Response recieved :<p><pre>" + response.getBody().asPrettyString() + "</pre></p>");

            assertEquals(response.getStatusCode(), expectedStatusCode, "Verify status code is " + expectedStatusCode + " for end point " + tempUrl, extentTest);

            driver.get(tempUrl);

            isServerError = driver.getTitle().contains("Server Error");
            assertEquals(isServerError, false, "Verifying server error message is not displayed", extentTest);
        }

    }

    @Test
    public void resultsEndpointTest() {
        ExtentTest extentTest = extent.createTest("6. results end point test");
        int expectedStatusCode = 200;
        String url = BASE_URL + "/api/results/";
        extentTest.pass("Doing GET : " + url);
        Response response = getResource(url, USERNAME, PASSWORD, AUTH_HEADER);
        extentTest.pass("Response recieved :<p><pre>" + response.getBody().asPrettyString() + "</pre></p>");
        assertEquals(response.getStatusCode(), expectedStatusCode, "Verify status code is " + expectedStatusCode, extentTest);
    }

    @Test
    public void gamesEndpointTest() {
        ExtentTest extentTest = extent.createTest("7. games end point test");
        int expectedStatusCode = 200;
        String url = BASE_URL + "/api/games/";
        extentTest.pass("Doing GET : " + url);
        Response response = getResource(url, USERNAME, PASSWORD, AUTH_HEADER);
        extentTest.pass("Response recieved :<p><pre>" + response.getBody().asPrettyString() + "</pre></p>");
        assertEquals(response.getStatusCode(), 200, "Verify status code is " + expectedStatusCode, extentTest);
    }
}
