/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import base.BaseOfTests;
import com.aventstack.extentreports.ExtentTest;
import java.util.List;
import lombok.Cleanup;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.SoccerResultsPage;

/**
 *
 * @author Veco Mxolisi
 */
public class WebPageTest extends BaseOfTests {

    @Test
    public void landingPageTest() {
        ExtentTest extentTest = extent.createTest("1. Landing page services test");

        @Cleanup("quit")
        WebDriver driver = initialiseDriver(BASE_URL + "/", extentTest);

        HomePage homePage = new HomePage(driver, extentTest);
        homePage.waitForPageToLoad(10000);

        String expectedStrategyAndInnovationHeader = "STRATEGY & INNOVATION";
        String expectedDesignAndDevelopmentHeader = "DESIGN & DEVELOPMENT";
        String expectedEvolutionAndAdoptionServiceHeader = "EVOLUTION & ADOPTION";

        assertEquals(homePage.getStrategyAndInnovationServiceHeader(), expectedStrategyAndInnovationHeader, "Verify service offering " + expectedStrategyAndInnovationHeader + " is displayed on home page", extentTest, driver);
        assertEquals(homePage.getDesignAndDevelopmentServiceHeader(), expectedDesignAndDevelopmentHeader, "Verify service offering " + expectedDesignAndDevelopmentHeader + " is displayed on home page", extentTest, driver);
        assertEquals(homePage.getEvolutionAndAdoptionServiceHeader(), expectedEvolutionAndAdoptionServiceHeader, "Verify service offering " + expectedEvolutionAndAdoptionServiceHeader + " is displayed on home page", extentTest, driver);
    }

    @Test
    public void redirectToLoginPageTest() {
        ExtentTest extentTest = extent.createTest("2. Landing page test");

        @Cleanup("quit")
        WebDriver driver = initialiseDriver(BASE_URL + "/soccer/results/", extentTest);
        LoginPage loginPage = new LoginPage(driver, extentTest);
        loginPage.waitForPageToLoad(10000);//if the username text field is not displayed after 10 seconds, an exception will be thrown, which would mean the login page was not displayed
    }

    @Test
    public void redirectToRequestedPageAfterLoginTest() {
        ExtentTest extentTest = extent.createTest("3. Redirect to requested Page after login test");

        String securedURL = BASE_URL + "/soccer/results/";
        @Cleanup("quit")
        WebDriver driver = initialiseDriver(securedURL, extentTest);
        LoginPage loginPage = new LoginPage(driver, extentTest);
        loginPage.waitForPageToLoad(10000);
        loginPage.login(USERNAME, PASSWORD);
        assertEquals(driver.getCurrentUrl(), securedURL, "Verifying user is redircted to the requested page after login", extentTest, driver);
    }

    @Test
    public void soccerResultsTest() {
        ExtentTest extentTest = extent.createTest("4. Soccer results page test");

        String securedURL = BASE_URL + "/soccer/results/";
        @Cleanup("quit")
        WebDriver driver = initialiseDriver(securedURL, extentTest);
        LoginPage loginPage = new LoginPage(driver, extentTest);
        loginPage.waitForPageToLoad(10000);
        loginPage.login(USERNAME, PASSWORD);

        SoccerResultsPage soccerResultsPage = new SoccerResultsPage(driver, extentTest);
        soccerResultsPage.waitForPageToLoad(10000);
        List<Integer> teamPoints = soccerResultsPage.getListOfTeamPoints();

        for (int i = 1; i < teamPoints.size(); i++) {
            assertEquals(teamPoints.get(i - 1) >= teamPoints.get(i), true, "Verifying results are ordered by score. Comparing row " + (i - 1) + "(with points " + teamPoints.get(i - 1) + ") and row " + i + "(with points " + teamPoints.get(i) + ")", extentTest, driver);
        }
    }

}
