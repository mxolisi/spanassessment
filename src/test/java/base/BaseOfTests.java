/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import io.github.bonigarcia.wdm.WebDriverManager;
import java.io.IOException;
import java.util.Base64;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import util.Helper;

/**
 *
 * @author Veco Mxolisi
 */
public class BaseOfTests {

    protected static ExtentReports extent;
    public final String BASE_URL = "http://127.0.0.1:8000";
    public final String USERNAME = "test";
    public final String PASSWORD = "test";
    public String AUTH_HEADER;

    @BeforeSuite(alwaysRun = true)
    public void setup() throws IOException {
        WebDriverManager.chromedriver().setup();
        extent = new ExtentReports();
        ExtentSparkReporter spark = new ExtentSparkReporter("resources/report/HTML_Test_Report.html");
        extent.attachReporter(spark);

        String auth = USERNAME + ":" + PASSWORD;
        AUTH_HEADER = "Basic " + Base64.getEncoder().encodeToString(auth.getBytes());
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() {
        extent.flush();
    }

    public WebDriver initialiseDriver(String url, ExtentTest extentTest) {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(url);
        extentTest.pass("Navigated to " + url);
        return driver;
    }

    public <T> void assertEquals(T actual, T expected, String message, ExtentTest extentTest, WebDriver driver) {
        try {
            Assert.assertEquals(actual, expected, message);
            extentTest.pass(message, Helper.takeBase64SreenShot(driver));
        } catch (AssertionError e) {
            extentTest.fail(e.getMessage(), Helper.takeBase64SreenShot(driver));
            throw e;
        }
    }

    public <T> void assertEquals(T actual, T expected, String message, ExtentTest extentTest) {
        try {
            Assert.assertEquals(actual, expected, message);
            extentTest.pass(message);
        } catch (AssertionError e) {
            extentTest.fail(e.getMessage());
            throw e;
        }
    }
}
