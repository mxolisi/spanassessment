This assessement was done on a Windows Operating System.
All other operating system should still be able to run the tests as long as they support the tools detailed in "Setup/Tools" section

Setup/Tools
    1. Install and setup JDK 8
    2. Install latest chrome web browser
    3. install and setup latest Maven
    4. install Git
    5. Access to the internet

Test execution
Note: you will need access to the internet when running below
    1. open the command line(CMD)
    2. run the command:  git clone https://mxolisi@bitbucket.org/mxolisi/spanassessment.git
    3. run the command:  mvn -f spanassessment/ clean install 

Test results/report
    After you complete the steps in "Test execution", the script will output a rich html report called "HTML_Test_Report.html" which contains the steps 
    taken by the tests.
    The file will be located in the folder under the project "spanassessment\resources\report\HTML_Test_Report.html".
